const { Client } = require('pg')

const http = require('http'),
	fs = require('fs');

      // server = http.createServer();

const db_client = new Client({
  user: 'gyan',
  host: '127.0.0.1',
  database: 'db_test',
  password: 'postgres',
  port: 5432
});

const web_port = 3000;

// Third party modules
const httpStatus = require('http-status-codes');


const routeMap = {
    "/": "out.html"
};

const server = http.createServer((request, response) => {
    response.writeHead(httpStatus.StatusCodes.OK, {
        "Content-Type": "text/html"
      });

    if (routeMap[request.url]) {
        fs.readFile(routeMap[request.url], (error, data) => {
            response.write(data);
            response.end();
        })
    } else {
        response.end("<h1>Sorry, page not found</h1>");
    }
})

server.listen(web_port);
console.log(`The server is listening on port: ${web_port}`);


/**
server.on('request',(request,response)=>{
   response.writeHead(200,{'Content-Type':'text/html'});
   response.write('out.html');
   response.end();
});

server.listen(3000,()=>{
  console.log('Node server created at port 3000');
});
**/

db_client.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});

