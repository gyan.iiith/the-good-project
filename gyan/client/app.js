const http = require('http'),
	fs = require('fs');

const web_port = 3001;

// Third party modules
const httpStatus = require('http-status-codes');

const routeMap = {
    "/": "out.html"
};

const server = http.createServer((request, response) => {
    response.writeHead(httpStatus.StatusCodes.OK, {
        "Content-Type": "text/html"
      });

    if (routeMap[request.url]) {
        fs.readFile(routeMap[request.url], (error, data) => {
            response.write(data);
            response.end();
        })
    } else {
        response.end("<h1>Sorry, page not found</h1>");
    }
});

server.listen(web_port);
console.log(`The server is listening on port: ${web_port}`);
