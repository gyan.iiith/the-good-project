const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;
const db = require('./queries');
const basePath = '/api';

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);


app.get(basePath + '/', (request, response) => {
    response.json({ info: 'Node.js, Express, and Postgres API' });
});

app.get(basePath + '/users', db.getUsers);
app.get(basePath + '/user/:id', db.getUserById);
app.post(basePath + '/users', db.createUser);
app.put(basePath + '/user/:id', db.updateUser);
app.delete(basePath + '/user/:id', db.deleteUser);

app.listen(port, () => {
    console.log(`App running on port ${port}.`);
});
