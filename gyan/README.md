- No need to bundle node_modules folder. It usually grows over time and always gets created when we run the command: 
    - `npm install`


# Steps to run the API
- Go to `api` folder.
- Run `npm install` or `npm i`.
- Run `node index.js`.

It should show message: `App running on port 3000.`

Now the APIs are accessible, for example, as: (Can also be run in the browser for GET endpoints, or consumed from a frontend app for all kinds.)
- GET http://localhost:3000/api/
- GET http://localhost:3000/api/users/
- GET http://localhost:3000/api/user/1

Other CRUD endpoints are in `/api/queries.js`. 



# Steps to run the client
- Go to `client` folder.
- Run `npm install` or `npm i`.
- Run `node app.js`.


# TODO
- To send `querystring` from `out.html` to hit API endpoints and receive the response back to `client`.
- Requires: 
    - native `fetch` library usage to send payload to endpoints starting with http://localhost:3000/api. 
    - receive response in a `JS` file associated with `out.html` to perform `request` and `response` actions.
    - Once `response` is received, `out.html` shall render the `response` accordingly.